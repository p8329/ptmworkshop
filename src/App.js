import './App.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import NavBar from './Components/NavBar';
import Landing from './Components/Landing'
import Program from './Components/Program';
import Attending from './Components/Attending';
import Submissions from './Components/Submissions';
import Proceedings from './Components/Proceedings';

function App() {
  return (

    <Router basename='/ptmworkshop'>
      <Routes>
        <Route path="/" element={<NavBar/>}>
          <Route index element={<Landing/>} />
          <Route path="program" element={<Program/>} />
          <Route path="attending" element={<Attending/>} />
          <Route path="submissions" element={<Submissions/>} />
          <Route path="proceedings" element={<Proceedings/>} />
        </Route>
      </Routes>
    </Router>
  );
}

export default App;
