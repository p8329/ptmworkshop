import React, { useState } from 'react';
import {Container, Navbar, Nav} from 'react-bootstrap';
import { Outlet, Link } from "react-router-dom";
import logo from './soups_logo.png';

export default function NavBar(props) {

    // auto-exand/close mobile navbar menu on selection
    const [expanded, setExpanded] = useState(false);

    return (
        <div>
         <Navbar className="navbar nav-pills" collapseOnSelect expand="lg" variant="light" fixed="top" expanded={expanded}>
            <Container>
                <Navbar.Brand className='nav-brand'>
                <img
                alt=""
                src={logo}
                width="40"
                height="40"
                className="d-inline-block"
                />{' '}
                SOUPS '22 - PTM</Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" onClick={() => setExpanded(!expanded)} />
            <Navbar.Collapse id="responsive-navbar-nav" aria-labelledby="responsive-navbar-nav" placement="end">
                <Nav className='ms-auto'>
                    <Nav.Link className="nav-link" as={Link} to={"/"} onClick={() => setExpanded(false)}>Overview</Nav.Link>
                    <Nav.Link className="nav-link" as={Link} to={"/program"} onClick={() => setExpanded(false)}>Program</Nav.Link>
                    <Nav.Link className="nav-link" as={Link} to={"/attending"} onClick={() => setExpanded(false)}>Attending</Nav.Link>
                    <Nav.Link className="nav-link" as={Link} to={"/submissions"} onClick={() => setExpanded(false)}>Submissions</Nav.Link>
                    <Nav.Link className="nav-link" as={Link} to={"/proceedings"} onClick={() => setExpanded(false)}>Proceedings</Nav.Link>
                </Nav>
            </Navbar.Collapse>
            </Container>
            
        </Navbar>

        <Outlet />
        </div>
    )
}