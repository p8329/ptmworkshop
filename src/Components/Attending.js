import React from 'react';

export default function Attending(props) {
    let attend1 = 'The Workshop on Privacy Threat Modeling will be held in conjunction with the ';
    let attend2 = ', August 7-9 in Boston, MA. If you plan to attend the workshop, please register for SOUPS.'
    let attend3 = 'We are waiting to hear back from USENIX about the specific location and date of the workshop. Please check back for this information – it will be updated as soon as we know.';
    let attend4 = 'Attendees must submit 2-page position papers to attend the workshop. All authors of submitted position papers will be invited to attend the workshop. Please see ';
    let attend5 = ' for details.';

    return (
        <div className='attending'>
            <div className='attending-details'>
                <h4 className='heading'>Attendance</h4>
                <p>{attend1} 
                <a className='link' href='https://www.usenix.org/conference/soups2022' target='_blank' rel="noopener noreferrer">Symposium on Usable Privacy and Security (SOUPS) 2022</a>
                {attend2}
                </p>
                <p>{attend3}</p>
                <p>{attend4}
                <a className='link' href='/submissions'>Submissions</a>
                {attend5}
                </p>
            </div>
            <div className='important-dates'>
                <h4 className='heading'>Important Dates</h4>
                <p>May 26, 2022 – Submission deadline</p>
                <p>June 9, 2022 – Invitation to attendees</p>
                <p>August 7-9, 2022 – The Symposium on Usable Privacy and Security</p>
            </div>
        </div>
    )
}