import React from 'react';

export default function Submissions(props) {
    let sub1 = 'The submission portal will be open soon. Please check back after May 1st, 2022.'
    let sub2 = 'We are soliciting short position papers (2 pages excluding references and/or appendix using the SOUPS formatting template, found '
    let sub3 = ' related to privacy attacks, threats, and threat modeling. These may address:'
    let subList = ['Positions, arguments, or perspectives',
                    'Proposals of processes, methodologies, or solutions',
                    'Novel ideas',
                    'Previously published results',
                    'Works-in progress',
                    'Case studies',
                    'Preliminary results']
    let sub4 = 'Submissions should include author names and affiliations. Workshop papers will be made available to attendees prior to the workshop and will be posted on the workshop website. These submissions will not be considered “published” work and should not preclude publication elsewhere.';
    let sub5 = 'All authors who submit position papers will receive an invitation to participate in the workshop. Evaluation of papers for determining invitations will only occur if there are an overwhelming number of submissions, in which case 35 selected authors will be invited to the workshop.';
    let sub6 = 'Please email ';
    let sub7 = ' with questions.'
    
    return (
        <div className='submissions'>
            <div className='submission-details'>
                <h4 className='heading'>Submissions</h4>
                <p className='bold-text'>{sub1}</p>
                <p> {sub2}
                <a className='link' href='https://www.usenix.org/conference/soups2022/call-for-papers' target='_blank' rel="noopener noreferrer">here</a>
                {sub3}
                </p>
                <ul className='bulleted-list'>
                {subList.map((subType, index) => (
                    <li>{subType}</li>
                ))}
                </ul>
                <p>{sub4}</p>
                <p>{sub5}</p>
                <p>{sub6}
                <a className='link' href='PTMworkshop@mitre.org'>PTMworkshop@mitre.org</a>
                {sub7}
                </p>
            </div>
            <div className='important-dates'>
                <h4 className='heading'>Important Dates</h4>
                <p>May 26, 2022 – Submission deadline</p>
                <p>June 9, 2022 – Invitation to attendees</p>
                <p>August 7-9, 2022 – The Symposium on Usable Privacy and Security</p>
            </div>
        </div>
    )
}