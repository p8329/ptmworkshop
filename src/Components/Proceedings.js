import React from 'react';

export default function Proceedings(props) {
    return (
        <div className='proceedings'>
            <h4 className='heading'>Proceedings</h4>
            <p>All position papers submitted to the workshop will be shared here. Check back after June 9th.</p>
        </div>
    )
}